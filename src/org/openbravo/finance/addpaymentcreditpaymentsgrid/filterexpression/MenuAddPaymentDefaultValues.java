/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package org.openbravo.finance.addpaymentcreditpaymentsgrid.filterexpression;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.advpaymentmngt.filterexpression.AddPaymentDefaultValuesHandler;
import org.openbravo.advpaymentmngt.utility.APRMConstants;
import org.openbravo.advpaymentmngt.utility.FIN_Utility;
import org.openbravo.client.kernel.ComponentProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBDateUtils;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.service.json.JsonUtils;

@ComponentProvider.Qualifier(APRMConstants.ADD_PAYMENT_MENU)
public class MenuAddPaymentDefaultValues extends AddPaymentDefaultValuesHandler {

  private static final long SEQUENCE = 100l;

  protected long getSeq() {
    return SEQUENCE;
  }

  @Override
  public String getDefaultExpectedAmount(Map<String, String> requestMap) throws JSONException {
    return BigDecimal.ZERO.toPlainString();
  }

  @Override
  public String getDefaultActualAmount(Map<String, String> requestMap) throws JSONException {
    return BigDecimal.ZERO.toPlainString();
  }

  @Override
  public String getDefaultIsSOTrx(Map<String, String> requestMap) throws JSONException {
    if (requestMap.containsKey("context")) {
      JSONObject context = new JSONObject(requestMap.get("context"));
      if (context.has("issotrx") && context.get("issotrx") != JSONObject.NULL) {
        return context.getBoolean("issotrx") ? "Y" : "N";
      }
    }
    return "Y";
  }

  @Override
  public String getDefaultTransactionType(Map<String, String> requestMap) {
    return "I";
  }

  @Override
  public String getDefaultPaymentType(Map<String, String> requestMap) throws JSONException {
    return "";
  }

  @Override
  public String getDefaultOrderType(Map<String, String> requestMap) throws JSONException {
    return "";
  }

  @Override
  public String getDefaultInvoiceType(Map<String, String> requestMap) throws JSONException {
    return "";
  }

  @Override
  public String getDefaultConversionRate(Map<String, String> requestMap) throws JSONException {
    return BigDecimal.ONE.toPlainString();
  }

  @Override
  public String getDefaultConvertedAmount(Map<String, String> requestMap) throws JSONException {
    return BigDecimal.ZERO.toPlainString();
  }

  @Override
  public String getDefaultReceivedFrom(Map<String, String> requestMap) throws JSONException {
    if (requestMap.containsKey("context")) {
      JSONObject context = new JSONObject(requestMap.get("context"));
      if (context.has("received_from") && context.get("received_from") != JSONObject.NULL
          && StringUtils.isNotEmpty(context.getString("received_from"))) {
        return context.getString("received_from");
      }
    }
    return "";
  }

  @Override
  public String getDefaultStandardPrecision(Map<String, String> requestMap) throws JSONException {
    return "";
  }

  @Override
  public String getDefaultCurrency(Map<String, String> requestMap) throws JSONException {
    if (requestMap.containsKey("context")) {
      JSONObject context = new JSONObject(requestMap.get("context"));
      if (context.has("c_currency_id") && context.get("c_currency_id") != JSONObject.NULL
          && StringUtils.isNotEmpty(context.getString("c_currency_id"))) {
        return context.getString("c_currency_id");
      }
    }
    String organization = getOrganization(requestMap);
    return OBContext.getOBContext().getOrganizationStructureProvider()
        .getLegalEntity(OBDal.getInstance().get(Organization.class, organization)).getCurrency()
        .getId();
  }

  @Override
  public String getOrganization(Map<String, String> requestMap) throws JSONException {
    if (requestMap.containsKey("context")) {
      JSONObject context = new JSONObject(requestMap.get("context"));
      if (context.has("ad_org_id") && context.get("ad_org_id") != JSONObject.NULL
          && StringUtils.isNotEmpty(context.getString("ad_org_id"))) {
        return context.getString("ad_org_id");
      }
    }
    Organization organization = OBContext.getOBContext().getCurrentOrganization();
    if ("0".equals(organization.getId())) {
      for (String org : OBContext.getOBContext().getReadableOrganizations()) {
        if (!"0".equals(org)) {
          organization = OBDal.getInstance().get(Organization.class, org);
          break;
        }
      }
    }
    return organization.getId();
  }

  @Override
  public String getDefaultPaymentMethod(Map<String, String> requestMap) throws JSONException {
    if (requestMap.containsKey("context")) {
      JSONObject context = new JSONObject(requestMap.get("context"));
      if (context.has("fin_paymentmethod_id")
          && context.get("fin_paymentmethod_id") != JSONObject.NULL
          && StringUtils.isNotEmpty(context.getString("fin_paymentmethod_id"))) {
        return context.getString("fin_paymentmethod_id");
      }
    }
    return "";
  }

  @Override
  public String getDefaultPaymentDate(Map<String, String> requestMap) throws JSONException {
    try {
      if (requestMap.containsKey("context")) {
        JSONObject context = new JSONObject(requestMap.get("context"));
        if (context.has("payment_date") && context.get("payment_date") != JSONObject.NULL
            && StringUtils.isNotEmpty(context.getString("payment_date"))) {
          Date paymentDate = JsonUtils.createDateFormat().parse(context.getString("payment_date"));
          return OBDateUtils.formatDate(paymentDate);
        }
      }
    } catch (Exception e) {
      return OBDateUtils.formatDate(new Date());
    }
    return OBDateUtils.formatDate(new Date());
  }

  @Override
  public String getDefaultDocument(Map<String, String> requestMap) throws JSONException {
    if (requestMap.containsKey("context")) {
      JSONObject context = new JSONObject(requestMap.get("context"));
      if (context.has("trxtype") && context.get("trxtype") != JSONObject.NULL
          && StringUtils.isNotEmpty(context.getString("trxtype"))) {
        return context.getString("trxtype");
      }
    }
    return "RCIN";
  }

  @Override
  public String getDefaultFinancialAccount(Map<String, String> requestMap) throws JSONException {
    if (requestMap.containsKey("context")) {
      JSONObject context = new JSONObject(requestMap.get("context"));
      if (context.has("fin_financial_account_id")
          && context.get("fin_financial_account_id") != JSONObject.NULL
          && StringUtils.isNotEmpty(context.getString("fin_financial_account_id"))) {
        return context.getString("fin_financial_account_id");
      }
    }
    return "";
  }

  @Override
  public String getDefaultCurrencyTo(Map<String, String> requestMap) throws JSONException {
    if (requestMap.containsKey("context")) {
      JSONObject context = new JSONObject(requestMap.get("context"));
      if (context.has("c_currency_to_id") && context.get("c_currency_to_id") != JSONObject.NULL
          && StringUtils.isNotEmpty(context.getString("c_currency_to_id"))) {
        return context.getString("c_currency_to_id");
      }
    }
    return "";
  }

  @Override
  public String getDefaultDocumentNo(Map<String, String> requestMap) throws JSONException {

    Organization org = OBDal.getInstance().get(Organization.class, getOrganization(requestMap));
    boolean isReceipt = "Y".equals(getDefaultIsSOTrx(requestMap));

    String strDocNo = FIN_Utility.getDocumentNo(org, isReceipt ? "ARR" : "APP", "FIN_Payment",
        false);

    return "<" + strDocNo + ">";
  }

  @Override
  public String getBankStatementLineAmount(Map<String, String> requestMap) throws JSONException {
    // BankStatementLineAmount
    return "";
  }
}
