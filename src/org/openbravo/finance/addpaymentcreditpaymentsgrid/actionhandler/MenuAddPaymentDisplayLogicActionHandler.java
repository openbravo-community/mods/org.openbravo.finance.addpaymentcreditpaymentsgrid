/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2015 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package org.openbravo.finance.addpaymentcreditpaymentsgrid.actionhandler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.client.application.DynamicExpressionParser;
import org.openbravo.client.application.Parameter;
import org.openbravo.client.application.Process;
import org.openbravo.client.application.RefWindow;
import org.openbravo.client.kernel.BaseActionHandler;
import org.openbravo.client.kernel.RequestContext;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.ad.domain.Reference;
import org.openbravo.model.ad.ui.Field;
import org.openbravo.model.ad.ui.Tab;
import org.openbravo.model.ad.ui.Window;
import org.openbravo.service.db.DalConnectionProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MenuAddPaymentDisplayLogicActionHandler extends BaseActionHandler {
  private static final Logger log = LoggerFactory
      .getLogger(MenuAddPaymentDisplayLogicActionHandler.class);
  private final String processDefinitionId = "9BED7889E1034FE68BD85D5D16857320";

  @Override
  protected final JSONObject execute(Map<String, Object> parameters, String data) {
    JSONObject result = new JSONObject();
    final Map<String, String> sessionAttributesMap = new HashMap<String, String>();
    OBContext.setAdminMode(true);
    try {
      for (Field field : getAddPaymentFieldsInGridWithDisplayLogic()) {

        if (field.getDisplayLogic() != null) {
          final DynamicExpressionParser parser = new DynamicExpressionParser(
              field.getDisplayLogic(), field.getTab(), field);
          setSessionAttributesFromParserResult(parser, sessionAttributesMap, field.getTab()
              .getWindow().getId());
        }

        if (field.getDisplaylogicgrid() != null) {
          final DynamicExpressionParser parser = new DynamicExpressionParser(
              field.getDisplaylogicgrid(), field.getTab(), field);
          setSessionAttributesFromParserResult(parser, sessionAttributesMap, field.getTab()
              .getWindow().getId());
        }
      }

      result.put("context", sessionAttributesMap);
    } catch (Exception e) {
      OBDal.getInstance().rollbackAndClose();
      log.error("Error executing MenuAddPaymentDisplayLogicActionHandler", e);
      try {
        result.put("context", sessionAttributesMap);
      } catch (JSONException e1) {
        log.error("Error executing MenuAddPaymentDisplayLogicActionHandler", e1);
      }
    } finally {
      OBContext.restorePreviousMode();
    }
    return result;
  }

  private List<Field> getAddPaymentFieldsInGridWithDisplayLogic() {
    StringBuffer where = new StringBuffer();
    where.append(" as f");
    where.append(" join f." + Field.PROPERTY_TAB + " as t");
    where.append(" join t." + Tab.PROPERTY_WINDOW + " as w");
    where.append(" join w." + Window.PROPERTY_OBUIAPPREFWINDOWLIST + " as rw");
    where.append(" join rw." + RefWindow.PROPERTY_REFERENCE + " as r");
    where.append(" join r." + Reference.PROPERTY_OBUIAPPPARAMETERREFERENCESEARCHKEYLIST + " as pp");
    where.append(" where pp." + Parameter.PROPERTY_OBUIAPPPROCESS + " = :process");
    where.append(" and f." + Field.PROPERTY_ACTIVE + " = true");
    where.append(" and f." + Field.PROPERTY_CLIENT + " = '0'");
    where.append(" and f." + Field.PROPERTY_ORGANIZATION + " = '0'");
    where.append(" and f." + Field.PROPERTY_DISPLAYED + " = true");
    where.append(" and (f." + Field.PROPERTY_DISPLAYLOGIC + " is not null");
    where.append(" or f." + Field.PROPERTY_DISPLAYLOGICGRID + " is not null)");

    OBQuery<Field> qry = OBDal.getInstance().createQuery(Field.class, where.toString());
    qry.setNamedParameter("process", OBDal.getInstance().get(Process.class, processDefinitionId));
    qry.setFilterOnActive(false);
    qry.setFilterOnReadableClients(false);
    qry.setFilterOnReadableOrganization(false);
    return qry.list();
  }

  /*
   * The request map is <String, Object> because includes the HTTP request and HTTP session, is not
   * required to handle process parameters
   */
  private void setSessionAttributesFromParserResult(DynamicExpressionParser parser,
      Map<String, String> sessionAttributesMap, String windowId) {
    String attribute = null, attrValue = null;
    for (String attrName : parser.getSessionAttributes()) {
      if (!sessionAttributesMap.containsKey(attrName)) {
        if (attrName.startsWith("inp_propertyField")) {
          // do not add the property fields to the session attributes to avoid overwriting its value
          // with an empty string
          continue;
        }
        if (attrName.startsWith("#")) {
          attribute = attrName.substring(1, attrName.length());
          attrValue = Utility.getContext(new DalConnectionProvider(false), RequestContext.get()
              .getVariablesSecureApp(), attribute, windowId);
        } else {
          attrValue = Utility.getContext(new DalConnectionProvider(false), RequestContext.get()
              .getVariablesSecureApp(), attrName, windowId);
        }
        sessionAttributesMap.put(attrName.startsWith("#") ? attrName.replace("#", "_") : attrName,
            attrValue);
      }
    }
  }
}