/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package org.openbravo.finance.addpaymentcreditpaymentsgrid.actionhandler;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.client.kernel.BaseActionHandler;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;

public class PaymentDescriptionActionHandler extends BaseActionHandler {

  @Override
  protected JSONObject execute(Map<String, Object> parameters, String data) {
    try {
      final String ad_org_id = (String) parameters.get("ad_org_id");
      final String issotrx = (String) parameters.get("issotrx");
      final String payment_documentno = (String) parameters.get("payment_documentno");
      String paymentNo = payment_documentno.substring(1, payment_documentno.length() - 1);
      JSONObject result = new JSONObject();
      String description = null;
      if (StringUtils.isNotEmpty(ad_org_id)) {
        Organization organization = OBDal.getInstance().get(Organization.class, ad_org_id);
        final OBCriteria<FIN_Payment> obc = OBDal.getInstance().createCriteria(FIN_Payment.class);
        obc.add(Restrictions.eq(FIN_Payment.PROPERTY_ORGANIZATION, organization));
        obc.add(Restrictions.eq(FIN_Payment.PROPERTY_RECEIPT, "true".equals(issotrx)));
        obc.add(Restrictions.eq(FIN_Payment.PROPERTY_DOCUMENTNO, paymentNo));
        final List<FIN_Payment> obcList = obc.list();
        if (obcList.size() > 0) {
          description = obcList.get(0).getDescription();
        }
      }
      result.put("payment_documentno", paymentNo);
      result.put("description", description);
      return result;
    } catch (Exception e) {
      throw new OBException(e);
    }
  }
}