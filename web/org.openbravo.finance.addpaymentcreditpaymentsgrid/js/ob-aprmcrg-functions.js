OB.APRM.AddPayment.originalOnLoad = OB.APRM.AddPayment.onLoad;
OB.APRM.AddPayment.onLoad = function(view) {
  OB.APRM.AddPayment.originalOnLoad(view);
  view.theForm.getItem('credit_payments').canvas.addNewButton.hide();

  if (!view.buttonOwnerView) {
    OB.APRM.AddPayment.getContext(function(returnValue) {
      view.additionalContextInfo = returnValue;
      view.handleDisplayLogicForGridColumns();
    });
  }
};

OB.APRM.AddPayment.getContext = function(callback) {
  var displayLogicCallback;

  displayLogicCallback = function(response, data, request) {
    callback(data.context);
  };

  OB.RemoteCallManager.call(
    'org.openbravo.finance.addpaymentcreditpaymentsgrid.actionhandler.MenuAddPaymentDisplayLogicActionHandler',
    {},
    {},
    displayLogicCallback
  );
};

OB.APRM.AddPayment.reloadParameters = function(paramObj) {
  var params, paymentDescriptionActionHandlerCallback;
  params = {
    ad_org_id: paramObj._processView.theForm.getItem('ad_org_id').getValue(),
    issotrx: paramObj._processView.theForm.getItem('issotrx').getValue(),
    payment_documentno: paramObj._processView.theForm
      .getItem('payment_documentno')
      .getValue(),
    paramObj: paramObj
  };
  paymentDescriptionActionHandlerCallback = function(response, data, request) {
    var defaultProcessActionHandlerCallback,
      params = request.params,
      context;
    if (data.description) {
      params.payment_documentno = data.payment_documentno;
      params.description = data.description;
      params.processId = request.params.paramObj._processView.processId;
      context = params.paramObj._processView.theForm.getValues();
    }

    defaultProcessActionHandlerCallback = function(response, data, request) {
      var form = request.params.paramObj._processView.theForm,
        view = request.params.paramObj._processView,
        creditPaymentGrid = form.getItem('credit_payments').canvas.viewGrid,
        orderInvoiceGrid = form.getItem('order_invoice').canvas.viewGrid,
        creditToUseGrid = form.getItem('credit_to_use').canvas.viewGrid,
        selectedRecords = creditToUseGrid.getSelectedRecords(),
        tab = OB.MainView.TabSet.getTab(view.viewTabId),
        affectedParams = [];
      var i = 0;
      for (i = 0; i < selectedRecords.length; i++) {
        creditPaymentGrid.addData({
          payment: request.params.payment_documentno,
          description: request.params.description,
          creditPayment: selectedRecords[i].documentNo
        });
      }
      creditToUseGrid.deselectAllRecords();
      orderInvoiceGrid.deselectAllRecords();
      tab.setTitle(view.tabTitle);
      affectedParams.push(form.getField('credit_to_use_display_logic').paramId);
      OB.APRM.AddPayment.recalcDisplayLogicOrReadOnlyLogic(
        form,
        view,
        affectedParams
      );
      view.handleDefaults(data);
    };
    OB.RemoteCallManager.call(
      'org.openbravo.client.application.process.DefaultsProcessActionHandler',
      context,
      params,
      defaultProcessActionHandlerCallback
    );
  };
  OB.RemoteCallManager.call(
    'org.openbravo.finance.addpaymentcreditpaymentsgrid.actionhandler.PaymentDescriptionActionHandler',
    {},
    params,
    paymentDescriptionActionHandlerCallback
  );
};
OB.Utilities.Action.set(
  'reloadParameters',
  OB.APRM.AddPayment.reloadParameters
);
